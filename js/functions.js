$(document).ready(function(){
    try {
   
    
        $('#modal1').modal({
            dismissible: false
        });   
        $('#modal1').modal('open');
    
        $("#btnClose").click(function (){
            localStorage.setItem('puzzle', 'abierto');
        });
        var isChrome = !!window.chrome && !!window.chrome.webstore;
        if(localStorage.getItem('puzzle') == 'abierto' || !isChrome ){ 
            $('#modal1').modal('close');
        }
    
        var $hamburger = $(".hamburger");
        $hamburger.on("click", function(e) {
            $hamburger.toggleClass("is-active");
            $(".menu").toggleClass("cerrado");
            $(".fondoMenu").toggleClass("cerrado");
        });
        
        $(".menu").find("li").on("click", function(e){
            $hamburger.trigger("click");
        } );
        
        $('.materialboxed').materialbox();
        
        $('.pushpin').pushpin();
        
        $('.collapsible').collapsible();
        var elem = document.querySelector('.collapsible.expandable');
        var instance = M.Collapsible.init(elem, {
        accordion: false
        });
        
        
        
    
        $("#cerrar-puzzle").on("click", function(){
            $(".puzzle-lightbox").remove();
        });
        
        $(".hash").on("mouseenter", function () {
            $(".hash").addClass("animated pulse");
        });
        $(".hash").on("mouseleave", function () {
            $(".hash").removeClass("animated pulse");
        });
        
        $("#fb").on("mouseenter", function () {
            $("#fb").addClass("animated  rubberBand");
        });
        $("#fb").on("mouseleave", function () {
            $("#fb").removeClass("animated rubberBand");
        });
    
        $("#tw").on("mouseenter", function () {
            $("#tw").addClass("animated rubberBand ");
        });
        $("#tw").on("mouseleave", function () {
            $("#tw").removeClass("animated rubberBand");
        });
    
        $("#yt").on("mouseenter", function () {
            $("#yt").addClass("animated rubberBand");
        });
        $("#yt").on("mouseleave", function () {
            $("#yt").removeClass("animated rubberBand");
        });
    
        $("#ig").on("mouseenter", function () {
            $("#ig").addClass("animated rubberBand");
        });
        $("#ig").on("mouseleave", function () {
            $("#ig").removeClass("animated rubberBand");
        });
    
        $(".horizontal").on("click", function(){
            location.href="index.html"
        });
        
        if ($(window).width() > 800) {
            $('.tarjeta-contenido:nth-child(1)').hover(function()  
            {        
                $(this).css("background-image", 
            "url(./assets/Detalles/OPH92N0.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/OPH92N0-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(2)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/355.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/355-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(3)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/434.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/434-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(4)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/1740.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/1740-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(5)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/271091-P5BN40-101.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/271091-P5BN40-101-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(6)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/OPH90B0.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/OPH90B0-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(7)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/OPH92N0.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/OPH92N0-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(8)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/OPH9350.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/OPH9350-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(9)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/355.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/355-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(10)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/434.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/434-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(11)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/1740.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/1740-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(12)').hover(function()  
            {        
                $(this).css("background-image", 
            "url(./assets/Detalles/OPH92N0.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/OPH92N0-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(13)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/355.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/355-gris.jpg)"); 
        });
        $('.tarjeta-contenido:nth-child(14)').hover(function() 
        {        
            $(this).css("background-image", 
            "url(./assets/Detalles/434.jpg)");
        }).mouseleave(function(){
            $(this).css("background-image", "url(./assets/Detalles/434-gris.jpg)"); 
        });
    }
    
    
        if ( $(window).width() < 800) {
            $(".tarjeta-contenido .activator").on("click", function(){
                return false;
            });
            $(window).scroll(function(){
                var wScroll = $(document).scrollTop();
                var limit = $(window).height()/10;
                var opacidad = 1-(wScroll/limit);
                var margen = 0;
                if (opacidad < 0) {
                    opacidad = 0;
                }
                $(".fecha").css("opacity", opacidad);
                $(".guitarra").css("opacity", opacidad);  
                // var altura = 90 - wScroll;  
                var altura = (90 - wScroll);  
                if (opacidad <= 0) {
                    $(".fecha").css("display", "none");
                } else {
                    $(".fecha").css("display", "");
                    
                }
                
                if (altura >= 20 && altura <= 90) {
                    $(".banner-inicio").css("height", altura +"vh" );    
                    $(".contenedor-marca").css("height", altura +"vh");
                    if (altura > 82) {
                        margen = 90;
                        $(".contenedor-marca").css("position", "");
                    }else{
                        $(".contenedor-marca").css("position", "fixed");
                        // $(".contenedor-marca").css("height", (altura) +"vh");
                        $(".contenedor-marca").css("height", (altura*0.5) +"vh");
                        margen = altura+8;
                    }
                    $(".progra").css("margin-top", (margen) + "vh");
                    $(".contenedor-marca").css("left", "0");
                    $(".contenedor-marca").css("right", "0");
                    // $(".contenedor-marca").css("top", (altura)+"vh");
                    $(".contenedor-marca").css("top", (altura*0.45)+"vh");
                    
                    
                }
                if(wScroll > 70){
                    $(".contenedor-marca").css("top", "9vh");
                    $(".contenedor-marca").css("height", "10vh");
                    $(".contenedor-marca").css("position", "fixed");
                    $(".banner-inicio").css("height", "20vh");
                }
            });
            
            
            
        } 
        

    } catch (error) {
        
    }    

});